export default {
  state: () => {
    return {
      tasks: []
    }
  },
  mutations: {
    set(state, tasks) {
      state.tasks = tasks
    }
  },
  actions: {
    /** Добавление нового задания */
    async add({ commit }, data) {
      await this.$axios.post("api/task", data)
    },

    /** Получение списка заданий */
    async get({ commit }) {
      const { data } = await this.$axios.get("api/task/user")
      commit("set", data)
    }
  }
}
