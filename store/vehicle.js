export default {
  state: () => {
    return {
      vehicle: {}
    }
  },
  mutations: {
    set(state, vehicle) {
      state.vehicle = vehicle
    }
  },
  actions: {
    /** Обновление qrcoda для транспортного средства с гаражным номером garageNumber */
    async update(store, { qrcode, garageNumber }) {
      await this.$axios.post(`api/vehicle/${garageNumber}`, { qrcode })
    },
    /** Получение информации о ТС по qrcode */
    async get_by_qrcode({ commit }, qrcode) {
      const response = await this.$axios.get(`api/vehicle/qrcode/${qrcode}`, {
        qrcode
      })
      commit("set", response.data)
    }
  }
}
