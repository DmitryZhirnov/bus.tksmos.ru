export default {
  actions: {
    /** Вход пользователя */
    async login(store, { username, password }) {
      await this.$auth.loginWith("local", {
        data: {
          grant_type: process.env.GRANT_TYPE,
          client_id: process.env.CLIENT_ID,
          client_secret: process.env.CLIENT_SECRET,
          username,
          password
        }
      })
    },

    /** Регистрация */
    async registration(store, { email, username, password }) {
      await this.$axios.post("api/user", {
        email,
        username,
        password
      })
    }
  }
}
