export default {
  state: () => {
    return {
      snackbar: {
        text: "",
        show: false,
        color: "#FFF"
      }
    }
  },
  mutations: {
    show(state, snackbar) {
      state.snackbar = { ...state.snackbar, ...snackbar }
    }
  }
}
