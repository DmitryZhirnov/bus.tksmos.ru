import colors from "vuetify/es5/util/colors"
require("dotenv").config()
console.log(process.env.NODE_ENV)
export default {
  mode: "spa",
  /*
   ** Headers of the page
   */
  head: {
    titleTemplate: "%s - " + process.env.npm_package_name,
    title: process.env.npm_package_name || "",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: process.env.npm_package_description || ""
      }
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: "#fff" },
  /*
   ** Global CSS
   */
  css: ["@/assets/app.scss"],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    "@plugins/axios",
    { src: "@/plugins/qr-code-reader.js", mode: "client" }
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    "@nuxtjs/eslint-module",
    // Doc: https://github.com/nuxt-community/stylelint-module
    // "@nuxtjs/stylelint-module",
    // Doc: https://github.com/nuxt-community/dotenv-module
    "@nuxtjs/dotenv",
    "@nuxtjs/vuetify"
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    "@nuxtjs/axios",
    "@nuxtjs/pwa",
    "@nuxtjs/proxy",
    "@nuxtjs/auth"
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */

  axios: {
    proxy: true,
    https: true,
    baseURL: process.env.LARAVEL_ENDPOINT
  },
  /** Proxy module config */
  proxy: {
    "/api": {
      target: process.env.LARAVEL_ENDPOINT,
      changeOrigin: true,
      secure: true
    },
    "/accessToken": {
      target: process.env.LARAVEL_ENDPOINT,
      secure: true,
      changeOrigin: true
    }
  },
  /** Auth module config */
  auth: {
    login: "/index",
    strategies: {
      local: {
        endpoints: {
          login: {
            url: "accessToken",
            method: "post",
            propertyName: "access_token"
          },
          user: { url: "api/auth/user", method: "get", propertyName: "" },
          logout: false
        },
        tokenRequired: true,
        tokenType: "Bearer",
        globalToken: true,
        autoFetchUser: true
      }
    }
  },
  /** Router settings  */
  router: {
    middleware: ["auth"]
  },
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  vuetify: {
    customVariables: ["~/assets/variables.scss"],
    theme: {
      dark: true,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {}
  },
  server: {
    port: 3000, // default: 3000
    host: "0.0.0.0"
  }
}
