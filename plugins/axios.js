export default function({ $axios, store, $auth, redirect }) {
  $axios.onError(({ response, request }) => {
    const code = parseInt(response && response.status)
    let text = ""
    switch (code) {
      case 422:
        const errors = JSON.parse(request.responseText)
        // разбираю ошибки в строку
        Object.keys(errors).every((error) => {
          errors[error].every((message) => {
            text += `${message}\r\n`
          })
        })
        break
      case 400:
        text = "Неверные логин или пароль"
        break
      case 401:
        text = response.data.message
        store.$auth.logout()
        redirect("/login")
        break
      default:
        text = response.data.message
        break
    }

    // Показываю ошибку */
    store.commit("snackbar/show", {
      text,
      show: true,
      color: "#A00"
    })
  })
}
