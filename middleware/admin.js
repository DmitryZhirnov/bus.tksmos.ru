export default function({ $auth, redirect }) {
  if ($auth?.user?.role !== "ADMIN") redirect("/login")
}
